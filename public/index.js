/////myBirth////
var myBirth = new Date('Octobre 25, 1996 11:15:00');

var dateNow = Date.now();

var ageMilli = dateNow - myBirth
var ageNormal = ageMilli / 31539999999

document.getElementById('years').innerText = parseInt(ageNormal) + " " + "ans";


////////////skills///////////
var animeDone = false;
window.addEventListener('scroll', function() {
  if (window.pageYOffset > 980 && animeDone == false) {
    makeProgress("barProb", 70);
    makeProgress("barTeam", 90);
    makeProgress("barData", 60);
    makeProgress("barPlanning", 75);
    makeProgress("barAdapt", 90);
    makeProgress("barProjects", 85);
    animeDone = true;
  }
});

console.log(pageYOffset);

function makeProgress(NameId, amountOfSkill) { //this function moves your bar to the amount (in %) of skill you put in. There are two parameters nameId = the name of your bar id and amountOfSkill is the percent of skill.
  //i is the size of the bar (in %) it is incremented with 1 each time
  var i = 1;
  var bar = document.getElementById(NameId); // var bar get the element with the id of <NameId>
  bar.classList.add("bar");

  setInterval(function() { // that changes the size of the bar every 100ms and that makes the bar moving
    if (i <= amountOfSkill) { // verify the condition that i is smaller or equal to one
      bar.setAttribute("style", "width:" + i + "%;"); // sets the with attribute on the bar.
      i++; //increments var i with 1.
    }
  }, 15);
}
